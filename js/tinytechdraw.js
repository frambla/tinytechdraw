/*
    Copyright 2020 Francesc Rambla i Marigot

    This file is part of tinyTechDraw.

    tinyTechDraw is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Foobar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Foobar.  If not, see <https://www.gnu.org/licenses/>.
*/


/*****************************************************************
 * Base Class for shapes
 */ 

class BaseShape {
   constructor(path) {
    this.path 			   = path;
    this.path.onMouseEnter   = this.onMouseEnter;
	  this.path.onMouseLeave   = this.onMouseLeave;
	  this.path.onClick		   = this.onClick;
	  this.shapes 			   = null;
	  this.path.data.baseShape = this; 
    this.penColor 		   = 'black';
    this.penWidth 		   = 1;
    this.lineType 		   = [];
    this.shapeType 		   = null;
   }
   set penColor(color) {
	  this.strokeColor = color;
	  this.path.strokeColor = color;
   }
   get penColor() {
	  return this.strokeColor;
   }
   set penWidth(width) {
	  this.strokeWidth = width;
	  this.path.strokeWidth = width;
   }
   get penWidth() {
	  return this.strokeWidth;
   }
   set lineType(type) {
	  this.__lineType__ = type;
	  this.path.dashArray = type;
   }
   get lineType() {
	  return this.__lineType__;
   }
   get selected() {
	   return this.path.selected;
   }
   set selected(selected) {
	   this.path.selected = selected;
   }
   delete() {
	 this.path.removeSegments();
   }
   onMouseEnter(event) {
	 var shapes = event.target.data.baseShape.shapes;
	 if (! shapes.waitingForPoints ) {
	   event.target.strokeWidth = 8;
	   event.target.strokeColor = '#a0a0ff';
	 }
   }
   onMouseLeave(event) {
	 var shape = event.target.data.baseShape;
	 var shapes = shape.shapes;
	 
	 if (! shapes.waitingForPoints ) {
 	   event.target.strokeWidth = shape.strokeWidth;
	   event.target.strokeColor = shape.strokeColor;
	 }
   }
   onClick(event) {
	 var shapes = event.target.data.baseShape.shapes;
 	 if ( shapes.waitingForLine ) {
 	   event.target.selected = !event.target.selected;
 	   event.stopPropagation();
 	   shapes.onClickView(event);
	 }
	 else if (! shapes.waitingForPoints ) {
 	   event.target.selected = !event.target.selected;
 	   event.stopPropagation();
 	 }
   }
}

/*****************************************************************
 * Line shape class
 */

class LineShape extends BaseShape {
	constructor(startPoint, endPoint) {
	  super(new paper.Path.Line(startPoint, endPoint));
	  this.startPoint = startPoint;
	  this.endPoint   = endPoint;
	  this.shapeType  = 'line';
	}
}

/*****************************************************************
 * Circle shape class
 */
 
class CircleShape extends BaseShape {
	constructor(center, radius) {
	  super(new paper.Path.Circle(center, radius));
	  this.center = center;
	  this.radius = radius;
	  this.shapeType  = 'circle';
	}
}

/*****************************************************************
 * Arc shape class
 */
 
class ArcShape extends BaseShape {
    constructor(center, startPoint, arcAngle) {
	  var v = startPoint.subtract(center);
	  var d = center.getDistance(startPoint);
	  var angle1 = v.angle + arcAngle/2;
	  var angle2 = v.angle + arcAngle;
    var clockwise = (angle2 < 0);
	  var middlePoint = new paper.Point({
		  length: d,
		  angle: angle1
	  });
	  var endPoint = new paper.Point({
		  length: d,
		  angle: angle2
	  });
	  super(new paper.Path.Arc({
	    from: startPoint, 
	    through: center.add(middlePoint), 
	    to: center.add(endPoint),
      clockwise: clockwise
	    }));
	  this.center     = center;
	  this.startPoint = startPoint;
	  this.endPoint   = endPoint;
	  this.radius	  = d;
	  this.shapeType  = 'arc';
	}
}

/*****************************************************************
 * References shapes class
 */

class ReferenceShapes {
   constructor() {
   this.baseLine    = null;
   this.line   		  = null;
	 this.arc    		  = null;
	 this.circle      = null;
	 this.strokeColor = '#a0a0ff';
	 this.dashArray   = [3, 2];
	 this.fontFamily	= 'Arial';
	 this.fontSize		= 9;
	 this.initialized = false;
   }
   _initPath() {
	  var p = new paper.Path();
	  p.strokeColor = this.strokeColor;
	  p.dashArray   = this.dashArray;
	  return p;
   }
   _initLabel() {
	  var t = new paper.PointText([0, 0]);
	  t.fontFamily  = this.fontFamily;
	  t.fontSize    = this.fontSize;
	  t.strokeColor = this.strokeColor;
	  return t;
   }
   init() {
     if (! this.initialized) {
       this.baseLine    = this._initPath();
       this.line        = this._initPath();
       this.arc         = this._initPath();
       this.circle      = this._initPath();
       this.lblLength   = this._initLabel();
       this.lblAngle    = this._initLabel();
       this.initialized = true;
     }
     else 
       this.clear();
   }	
   drawBaseLine(p1, p2) {
     this.baseLine.removeSegments();
     this.baseLine.add(p1);
     this.baseLine.add(p2);
   }
   drawLine(p1, p2) {
     this.line.removeSegments();
     this.line.add(p1);
     this.line.add(p2);
   }
   drawCircle(c, radius) {
     this.circle.removeSegments();
     var p1 = c.clone();
     var p2 = c.clone();
     p1.length -= radius;
     p2.length += radius;
     this.drawBaseLine(c, p2);
     this.circle.add(p1);
     this.circle.arcTo(p2);
     this.circle.arcTo(p1);
   }
   drawArc(c, p, angle, counterwise=false) {
     this.arc.removeSegments();
     var l = c.getDistance(p);
     var a = p.subtract(c).angle;
     var a1 = a + angle / 2;
     var a2 = a + angle;
     //a1 = a1 > 180 ? a1 - 360 : a1;
     //a2 = a2 > 180 ? a2 - 360 : a2;
     var middlePoint = c.add(new paper.Point({
        length: l,
        angle: a1
        }));
     var endPoint = c.add(new paper.Point({
        length: l,
        angle: a2
        }));
     
     this.arc.counterwise = counterwise; 
     this.drawBaseLine(c, p);
     this.arc.add(p);
       this.arc.arcTo(middlePoint, endPoint);
   } 
   showLengthLabel(length) {
     this.lblLength.position = this.line.position;
     this.lblLength.content = length.toFixed(2);  
   }
   showAngleLabel(angle) {
     this.lblAngle.position = this.arc.position;
     this.lblAngle.content = -angle;
   }
   clear() {
     this.baseLine.removeSegments();
     this.line.removeSegments();
     this.circle.removeSegments();
     this.arc.removeSegments();
     this.lblLength.content = '';
     this.lblAngle.content = '';
   }
}

class ReferenceGuide {
  constructor() {
	this.__endingPoint__ = null;
	this.__angle__		   = null;
	this.__length__      = null;
  this.__counterwise__ = false;
	this.startingPoint   = null;
	this.centerPoint	   = null;
	this.fixedPoint		   = false;
	this.fixedLength     = false;
	this.fixedAngle		   = false;
	this.fixedCenter     = false;
	this.refShapeType    = 'line';
  // Shape initialization
  this.refShapes       = new ReferenceShapes();
  }
  get endingPoint() {
	return this.__endingPoint__;  
  }
  set endingPoint(point) {
	updateReference(point);
  }
  get length() {
	return this.__length__;
  }
  get angle() {
    return this.__angle__;
  }
  __updateReference() {
	if (! this.refShapes.initialized ) {
	  this.refShapes.init();
	}
	switch (this.refShapeType) {
	  case 'line': 
	    if (this.fixedCenter) 
	      this.refShapes.drawLine(this.centerPoint, this.__endingPoint__);
	    else
	      this.refShapes.drawLine(this.startingPoint, this.__endingPoint__);
	    this.refShapes.showLengthLabel(this.__length__);
	    break;
	  case 'circle':
	    this.refShapes.drawLine(this.centerPoint, this.__endingPoint__);
	    this.refShapes.showLengthLabel(this.__length__);
	    this.refShapes.drawCircle(this.centerPoint, this.__length__);
	    break;
	  case 'arc':
	    if (this.fixedPoint) 
	      this.refShapes.drawLine(this.centerPoint, this.startingPoint)
	    else
	      this.refShapes.drawLine(this.centerPoint, this.__endingPoint__);
	    this.refShapes.showLengthLabel(this.__length__);
      this.refShapes.drawArc(this.centerPoint, this.startingPoint, this.__angle__, this.__counterwise__);
	    this.refShapes.showAngleLabel(this.__angle__);
	    break;
    }
  }
  fixPoint(point) {
    this.startingPoint = point;
    if (! this.fixedCenter) 
      this.centerPoint = this.startingPoint;
	this.fixedPoint    = true;
  }
  fixCenter(point) {
    this.centerPoint = point;
    this.fixedCenter = true;
  }
  fixAngle(angle) {
    this.__angle__ = angle;
    if ((angle >= 0) && (angle <= 90))
      this.quadrant = 1;
    else if ((angle > 90) && (angle <= 180))
        this.quadrant = 2;
      else if ((angle > 180) && (angle <= 270))
        this.quadrant = 3;
      else
        this.quadrant = 4;
    this.fixedAngle = true;
  }
  fixLength(point, length) {
	this.__length__ = length;
	this.startingPoint = point;
	this.fixedDistance = true;
  }
  getShortestAngle(angle) {
    if (Math.abs(angle) > 180) {
      var newAngle = 360 - Math.abs(angle);
      //if (angle < 0) newAngle *= -1;
      return newAngle;
    }
    return angle;
  }
  updateReference(point) {
	if ( this.fixedPoint || this.fixedCenter ) {
	  if ( this.fixedDistance ) {
	    this.__angle__ = Math.round(
	      this.centerPoint.subtract(point).angle -
	      this.centerPoint.subtract(this.startingPoint).angle);
      var d = this.centerPoint.getDistance(point);
      if (d < this.__length__)
        this.__angle__ = this.getShortestAngle(this.__angle__);
	    this.__endingPoint__ = this.centerPoint.add(new paper.Point({
		    length: this.__length__,
		    angle:  this.__angle__
	    }));
	  } else if ( this.fixedAngle ) {
	    this.__length__ = Math.round(this.centerPoint.getDistance(point));
	    if (point.quadrant != this.quadrant) 
	      this.fixAngle((this.__angle__ + 180) % 360);
	    this.__endingPoint__ = this.centerPoint.add(new paper.Point({
	  	  length: this.__length__,
		     angle:  this.__angle__
	    }));  	
	  } else {
	    this.__endingPoint__ = point;
	    if ( this.fixedPoint) 
	      this.__length__ = Math.round(this.startingPoint.getDistance(point));
	    if ( this.centerPoint)
	      this.__length__ = Math.round(this.centerPoint.getDistance(point));
	  }
	  this.__updateReference();
    } 
  }
  referenceLine() {
	  this.refShapeType = 'line';
  }
  referenceCircle() {
	  this.refShapeType = 'circle';
  }
  referenceArc() {
	  this.refShapeType = 'arc';
  }
  clear() {
	this.fixedPoint    = false;
	this.fixedDistance = false;
	this.fixedAngle    = false;
	this.fixedCenter   = false;
	this.refShapes.clear();
  }
}

class DrawingShapes {
	constructor() {
	  this.shapes 			= [];	
	  this.intersections 	= [];
	  this.selectedPoint 	= null;
	  this.waitingForPoints = false;
	  this.waitingForLine   = false;
	  this.waitingCallback	= null;
	  this.points 			= [];
	  this.referenceLine	= null;
	  this.guide	        = new ReferenceGuide();
	  this.penColor			= 'black';
	  this.penWidth			= 1;
	  this.lineType			= [];
	  this.fixedAngle		= false;
	  this.fixedStartingPoint = false;
	  this.referenceAngle	= null;
	  this.referencePoint	= null;
	}
// Selection
   unselectAll() {
	 while (paper.project.selectedItems.length > 0) 
		paper.project.selectedItems[0].selected = false;
   }
// Status
   get waitingForPoints() {
	 return this._waitingForPoints;
   }
   set waitingForPoints (waitingForPoints) {
	 this._waitingForPoints = waitingForPoints;
   }
// Pen
   get penColor() {
	 return this.__penColor__;
   }
   set penColor(color) {
	 this.__penColor__ = color;
   }
   get penWidth() {
	 return this.__penWidth;
   }
   set penWidth(width) {
	 this.__penWidth = width;
   }
   get lineType() {
	 return this.__lineType__;
   }
   set lineType(type) {
	 this.__lineType__ = type;
   }
// Intersections
	getIntersections(shape1, shape2) {
      var ints = shape1.path.getCrossings(shape2.path);
      for (var i = 0; i < ints.length; i++) {
		this.intersections[this.intersections.length] = {
		  point: ints[i].point,
		  path: null
	    };
      }
    }
	showIntersections() {
	  for (var i = 0; i < this.shapes.length; i++) {
		for (var j = i + 1; j < this.shapes.length; j++) {
		   this.getIntersections(this.shapes[i], this.shapes[j]);
		}
	  }
	  for (var i = 0; i < this.intersections.length; i++) {
        this.intersections[i].path = new paper.Path.Circle({
            center: this.intersections[i].point,
            radius: 5,
            fillColor: '#009dec',
            opacity: 0.5,
            onMouseEnter: this.onMouseEnterIntersection,
            onMouseLeave: this.onMouseLeaveIntersection,
            onClick: this.onClickIntersection,
            data: {
				shapes: this
			}
        });
	  }
	}
	hideIntersections() {
	  while (this.intersections.length > 0) {
		 this.intersections[0].path.removeSegments();
		 this.intersections[0].path.remove();
		 this.intersections[0].point = undefined;
		 this.intersections.shift();
	  }
	}
	onMouseEnterIntersection(event) {
	  event.target.strokeColor = 'black';
	}
	onMouseLeaveIntersection(event) {
	  event.target.strokeColor = null;
	}
	onClickIntersection(event) {
	  var shapes = event.target.data.shapes;
	  event.target.fillColor = 'red';
	  shapes.selectedPoint = event.target.position;
	  if (shapes.waitingForPoints) {
		 shapes.points[shapes.points.length] = shapes.selectedPoint;
	     if (shapes.waitingCallback)
	       shapes.waitingCallback(shapes.points);
	     event.stopPropagation();
	  }
	}
// Point selection
    waitForLine(callback) {
	  if (!this.waitingForLine) {
		this.points = [];
		this.waitingForLine = true;
		this.hideIntersections();
	  }
	  this.waitingCallback = callback;
	}
	stopWaitingForLine() {
	  this.waitingForLine = false;
	}
    waitForPoint(callback) {
	  if (!this.waitingForPoints) {
		this.points = [];
		this.waitingForPoints = true;
		this.hideIntersections();
  	    this.showIntersections();
	  }
	  this.waitingCallback = callback;
	}
	stopWaitingForPoint() {
	  this.hideIntersections();
	  this.fixedStartingPoint = false;
	  this.waitingForPoints = false;
	}
    onMouseMoveView(event) {
	  if (this.waitingForPoints) {
		this.guide.updateReference(event.point);
	  }
	}
    onClickView(event) {
      if (this.waitingForPoints) {
	    this.points[this.points.length] = event.point;
	    if (this.waitingCallback) {
			this.waitingCallback(this.points);
		}
	  }
	  if (this.waitingForLine) {
		for (var i=0; i<this.shapes.length; i++) {
		  if (this.shapes[i].selected && (this.shapes[i].shapeType == 'line'))
		    this.waitingCallback([this.shapes[i].startPoint, this.shapes[i].endPoint]);
		}
	  }
	}
// Shapes
    getShapeById(id) {
	  for (var i=0; i<this.shapes.length; i++) {
		 if (this.shapes[i].id == id)
		   return this.shapes[i];
	  }
	}
	addShape(shape) {
	  this.shapes[this.shapes.length] = shape;
	  shape.shapes = this;
	  shape.penColor = this.penColor;
	  shape.penWidth = this.penWidth;
	  shape.lineType = this.lineType;
	}
	addLine(startPoint, endPoint) {
	  this.addShape(new LineShape(startPoint, endPoint));
	}
	addCircle(center, radius) {
	  this.addShape(new CircleShape(center, radius));
	}
	addArc(center, startPoint, angle) {
	  this.addShape(new ArcShape(center, startPoint, angle));
	}
// User interaction
  getTwoPoints(points) {
	  if (points) {
		switch (points.length) {
		  case 0:
		    this.waitForPoint(this.getTwoPoints);
		    break;
		  case 1: 
	        this.guide.fixPoint(points[0]);
		    this.guide.referenceLine(); 
		    this.waitForPoint(this.getTwoPoints);
	        break;
	      case 2:
          this.stopWaitingForPoint(); this.guide.clear(); 
          this.getTwoPointsCallback(points[0], points[1]); break;
		}
	  }
	}
	getCenterRadius(points) {
	  if (points) {
		switch (points.length) {
		  case 0:
		    this.waitForPoint(this.getCenterRadius);
		    break;
		  case 1:
		    this.guide.fixCenter(points[0]);
		    this.guide.referenceCircle();
		    this.waitForPoint(this.getCenterRadius);
		    break;
		  case 2:
		    this.stopWaitingForPoint();
		    this.guide.clear();
		    this.getCenterRadiusCallback(this.guide.centerPoint, this.guide.length);
		    break;
		}
	  }
	}
	getCenterRadiusAngle(points) {
	  if (points) {
      switch (points.length) {
        case 0:
          this.waitForPoint(this.getCenterRadiusAngle);
          break;
        case 1:
          this.guide.fixCenter(points[0]);
          this.guide.referenceLine();
          this.waitForPoint(this.getCenterRadiusAngle);
          break;
        case 2:
          this.guide.fixPoint(points[1]);
          this.guide.fixLength(points[1], points[0].getDistance(points[1]));
          this.guide.referenceArc();
          this.waitForPoint(this.getCenterRadiusAngle);
          break;
        case 3:
          this.stopWaitingForPoint();
          this.guide.clear();
          this.getCenterRadiusAngleCallback(
            this.guide.centerPoint, 
            this.guide.startingPoint,
            this.guide.angle
            );
          break;
      }
	  }
  }
	getLineStartPointAndLength(points) {
	  if (points) {
      switch (points.length) {
        case 0:
          this.waitForLine(this.getLineStartPointAndLength);
          break;
        case 1:
        this.guide.fixCenter(points[0]);
        this.guide.referenceLine();
        this.waitForPoint(this.getLineStartPointAndLength);
        break;
        case 2:
          if ( this.waitingForLine ) {
            var p = new paper.Point(points[1]);
            var a = p.subtract(points[0]).angle;
            if (this.parallel)
              this.guide.fixAngle(a);
            if (this.orto)
              this.guide.fixAngle(a + 90);
            this.stopWaitingForLine();
            this.waitForPoint(this.getLineStartPointAndLength);
          }
          else {
            this.stopWaitingForPoint();
            this.guide.clear();
            this.getLineStartPointAndLengthCallback(this.guide.centerPoint, this.guide.angle, this.guide.length);
          }
          break;   
      }
	  }
	}
  drawLine(p1=null, p2=null) {
	  if (p1 && p2) {
      this.addLine(p1, p2);
      this.drawLine([]);  
	  }
	  else {
      this.getTwoPointsCallback = this.drawLine;
      this.getTwoPoints([]);
	  }
	}
	drawParallelOrto(p1=null, a=null, l=null) {
	  if ((p1 != null) && (a != null) && (l != null)) {
      var p2 = p1.add(new paper.Point({ length: l, angle: a}));
      this.addLine(p1, p2);
      this.drawParallelOrto();
	  }
	  else {
      this.getLineStartPointAndLengthCallback = this.drawParallelOrto;
      this.getLineStartPointAndLength([]);
	  }
	}
	drawParallel(p1=null, a=null, l=null) {
	   this.parallel = true;
	   this.orto = false;
	   this.drawParallelOrto(p1, a, l);
	}
	drawOrto(p1=null, a=null, l=null) {
	   this.parallel = false;
	   this.orto = true;
	   this.drawParallelOrto(p1, a, l);
	}
	drawCircle(center=null, radius=null) {
	  if (center && radius) {
	    this.addCircle(center, radius);
      this.drawCircle();
	  }
	  else {
		this.getCenterRadiusCallback = this.drawCircle;
		this.getCenterRadius([]);
	  }
	}
	drawArc(center = null, p1 = null, angle = null) {
	  if (center && p1 && angle) {
	    this.addArc(center, p1, angle)
      this.drawArc();
	  }
	  else {
	    this.getCenterRadiusAngleCallback = this.drawArc;
	    this.getCenterRadiusAngle([]);
	  }
	}
	cancel() {
	  this.stopWaitingForPoint();
	  this.guide.clear();
	}
	delete() {
    var i = 0;
	  while (i<this.shapes.length) {
		  if (this.shapes[i].selected) {
			  this.shapes[i].delete();
			  this.shapes.splice(i, 1);
		  }
		  else 
		    i++;
	  }
	}
}
/*
class DrawingGrid {
	constructor() {
	}
}
*/
class TinyTechDrawApp {
  constructor() {
    this.draw    = new DrawingShapes();
    this.guide   = new ReferenceGuide();
  }
  
}

